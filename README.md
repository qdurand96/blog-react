# Projet Blog

Pour ce projet, nous devions créer entièrement une appli de type Blog. Je n'ai pas choisi de theme particulier, et me suis concentré sur le fait de rajouter quelques fonctionnalités, comme les commentaires en dessous des posts, et un systeme de like inspiré des réseaux sociaux. 

Les nouveautés du projet étaient notamment, côté back, le hashing des mots de passe et l'ajout des routes sécurisées via passport/JWT, qui nous permettent, en plus de sécuriser les routes, d'utiliser les informations de l'utilisateur actuellement connecté (par exemple pour définir des permissions en fonction des rôles).

Côté front, la principale différence réside dans l'utilisation de redux, qui nous permet de gérer un state global pour l'application. Ici, seule une partie du projet utilise redux (l'authentification et le fetch des posts), mais avec du recul, l'idéal aurait été de l'utiliser de partout, ce qui donnerait un code beaucoup plus lisible.

### Maquettes

Version desktop: 

![alt text](public/img/Maquette Desktop.png)

Version mobile:

![alt text](public/img/Maquette Mobile.png)

### User Stories

![alt text](public/img/User Story Lecteur.png)
![alt text](public/img/User Story Contributeur.png)

### Diagramme de classes

![alt text](public/img/Class diagram.jpeg)

**Lien gitlab du back:** <https://gitlab.com/qdurand96/blog-api>

**Lien de l'appli:** <https://quentin-blog.netlify.app/>
