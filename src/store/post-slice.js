import { createSlice } from '@reduxjs/toolkit'
import { PostService } from '../services/PostService';

const postSlice = createSlice({
    name: 'post',
    initialState: {
        allPosts:[],
        myPosts:[]
    },
    reducers: {
        setAllPosts(state,action){
            state.allPosts=action.payload
        },
        setMyPosts(state,action) {
            state.myPosts=action.payload
        }
    }
})

export const { setAllPosts,setMyPosts } = postSlice.actions

export default postSlice.reducer

export const fetchAllPosts = () => async (dispatch)=>{
    try {
        const posts = await PostService.getAllPosts()
        dispatch(setAllPosts(posts))
    } catch (error) {
        console.log(error);
    }
}

export const fetchMyPosts = () => async (dispatch)=>{
    try {
        const posts = await PostService.getUserPost()
        dispatch(setMyPosts(posts))
    } catch (error) {
        console.log(error);
    }
}

export const addPost = (values) => async(dispatch)=>{
    try {
        await PostService.add(values)
        dispatch(fetchAllPosts())
        dispatch(fetchMyPosts())
    } catch (error) {
        console.log(error);
    }
}

export const updatePost = (id,values) => async (dispatch)=>{
    try {
        await PostService.update(id,values)
        dispatch(fetchAllPosts())
        dispatch(fetchMyPosts())
    } catch (error) {
        console.log(error);
    }
}

export const deletePost = (id) => async(dispatch)=>{
    try {
        await PostService.delete(id)
        dispatch(fetchAllPosts())
        dispatch(fetchMyPosts())
    } catch (error) {
        console.log(error);
    }
}