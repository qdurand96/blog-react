import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./auth-slice";
import postSlice from "./post-slice";

export const store = configureStore({
    reducer: {
        auth: authSlice,
        posts: postSlice
    }
});