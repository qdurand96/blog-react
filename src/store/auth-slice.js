import { createSlice } from '@reduxjs/toolkit'
import { AuthService } from '../services/AuthService';
import { fetchAllPosts, fetchMyPosts } from './post-slice';

const authSlice = createSlice({
    name: 'user',
    initialState: {
        user:null,
        loginFeedback:'',
        registerFeedback:''
    },
    reducers: {
        login(state, action) {
            const user = action.payload;
            state.user = user;
        },
        logout(state) {
            state.user=null;
            localStorage.removeItem('token')
        },
        updateLoginFeedback(state,action){
            state.loginFeedback=action.payload
        },
        updateRegisterFeedback(state,action){
            state.registerFeedback=action.payload
        },
        update(state,action){
            state.user = action.payload
        }
    }
})

export const { login, logout, updateLoginFeedback, updateRegisterFeedback, update } = authSlice.actions

export default authSlice.reducer

export const loginWithToken = () => async (dispatch) => {
    try {
        const token = localStorage.getItem('token')
        if(!token){
            return
        }
        const user = await AuthService.fetchAccount()
        dispatch(login(user));
    } catch (error) {
        console.log(error);
        dispatch(logout())
    }
}

export const register = (body) => async(dispatch)=> {
    try {
        await AuthService.register(body);
        dispatch(updateRegisterFeedback('Success'));
        dispatch(loginWithCredentials(body));
    } catch (error) {
        dispatch(updateRegisterFeedback(error.response.data.error))
    }
}

export const loginWithCredentials = (credentials) => async(dispatch)=>{
    try {
        const user = await AuthService.login(credentials);
        dispatch(login(user))
        dispatch(updateLoginFeedback('Logged in'))
    } catch (error) {
        dispatch(updateLoginFeedback('Credentials error'))
    }
}

export const updateUser = (values) => async (dispatch)=>{
    try {
        const user = await AuthService.updateAccount(values)
        dispatch(update(user))
        dispatch(fetchAllPosts())
        dispatch(fetchMyPosts())
    } catch (error) {
        console.log(error);
    }
}