import { Switch, Route } from 'react-router-dom';
import { useDispatch } from "react-redux";
import { Nav } from "./components/Nav";
import { Home } from "./pages/Home";
import { Account } from "./pages/Account";
import { useEffect } from 'react';
import { loginWithToken } from './store/auth-slice';
import { Register } from './pages/Register';
import { Login } from './pages/Login';
import { MyPosts } from './pages/MyPosts';
import { ProtectedRoute } from './components/ProtectedRoute';
import { fetchAllPosts, fetchMyPosts } from './store/post-slice';
import { PostDetails } from './components/PostDetails';

function App() {

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loginWithToken())
    dispatch(fetchAllPosts())
    dispatch(fetchMyPosts())
  },[dispatch])

  return (
    <>
      <Nav />

      <Switch>

        <Route path='/' exact>
          <Home />
        </Route>

        <ProtectedRoute path='/myposts'>
          <MyPosts/>
        </ProtectedRoute>

        {/* <Route path="/post/:id" children={<PostDetails/>} /> */}
        <Route path='/post/:id'>
          <PostDetails/>
        </Route>

        <ProtectedRoute path='/account'>
          <Account />
        </ProtectedRoute>

        <Route path='/register'>
          <Register />
        </Route>

        <Route path='/login'>
          <Login />
        </Route>
        
      </Switch>
    </>


  );
}

export default App;
