import axios from "axios";


export class CommentService {

    static async add(comment) {
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/comment',comment)
        return response.data
    }

    static async update(id, comment) {
        const response = await axios.patch(process.env.REACT_APP_SERVER_URL+'/api/comment/'+id,comment)
        return response.data
    }

    static async delete(id){
        await axios.delete(process.env.REACT_APP_SERVER_URL+'/api/comment/'+id)
    }

    static async getOneComment(id){
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/comment/'+id)
        return response.data
    }

    static async like(id){
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/comment/'+id+'/like')
        return response.data
    }

    static async isLiked(id){
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/comment/'+id+'/like')
        return response.data
    }
}