import axios from "axios";


export class PostService {

    static async add(post) {
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/post',post)
        return response.data
    }

    static async update(id, post) {
        const response = await axios.patch(process.env.REACT_APP_SERVER_URL+'/api/post/'+id,post)
        return response.data
    }

    static async delete(id){
        await axios.delete(process.env.REACT_APP_SERVER_URL+'/api/post/'+id)
    }

    static async getAllPosts(){
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/post')
        return response.data
    }

    static async getOnePost(id){
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/post/'+id)
        return response.data
    }

    static async getUserPost(){
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/post/myposts/all')
        return response.data
    }

    static async getPostComments(id){
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/post/'+id+'/comment')
        return response.data
    }

    static async like(id){
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/post/'+id+'/like')
        return response.data
    }

    static async isLiked(id){
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/post/'+id+'/like')
        return response.data
    }
}