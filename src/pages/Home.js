import { Col, Row } from "antd";
import { useSelector } from "react-redux";
import { NewPost } from "../components/NewPost";
import { Post } from "../components/Post";



export function Home() {


    const posts = useSelector(state => state.posts.allPosts)
    const user = useSelector(state => state.auth.user)

    return (
        <>
            {user &&
                <NewPost />
            }

            <Row justify="space-around">
                {posts.map(item => <Col key={item.id} xs={10} md={5}><Post post={item} /></Col>)}

            </Row>
        </>
    )
}