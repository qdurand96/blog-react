import { useDispatch } from "react-redux";
import { AccountForm } from "../components/AccountForm";
import { updateUser } from "../store/auth-slice";


export function Account(){

    const dispatch = useDispatch()

    async function updateAccount(values){
        dispatch(updateUser(values))
    }

    return(
        <AccountForm onSubmit={updateAccount}/>
    )
}