import { Button, Card } from "antd"
import { useEffect, useState } from "react"
import { CommentService } from "../services/CommentService"
import { LikeOutlined } from '@ant-design/icons';
import { useSelector } from "react-redux";
import { CommentForm } from "./CommentForm";



export function Comment({ comment, onUpdate, onDelete }) {

    const [isLiked, setIsLiked] = useState(false)
    const [likes, setLikes] = useState(comment.like)
    const [open, setOpen] = useState(false)

    const user = useSelector(state => state.auth.user)

    useEffect(() => {
        fetchLike()
    })

    async function like() {
        await CommentService.like(comment.id)
        if (isLiked === true) {
            setLikes(likes - 1)
        } else {
            setLikes(likes + 1)
        }
        fetchLike()

    }

    async function fetchLike() {
        const liked = await CommentService.isLiked(comment.id)
        setIsLiked(liked)
    }

    function update(values) {
        onUpdate(comment.id, values)
        setOpen(false)
    }

    async function deleteThis() {
        onDelete(comment.id)
    }

    return (
        <>
            <Card style={{ width: '100%' }}>
                {user.id === comment.userId &&
                    <div>
                        {open ? <CommentForm onSubmit={update} /> : <Button onClick={() => setOpen(true)}>Update</Button>}
                        <Button onClick={() => deleteThis()}>X</Button>
                    </div>
                }
                <p>{new Intl.DateTimeFormat('fr-FR', { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: "2-digit", minute: "2-digit", second: "2-digit" }).format(new Date(comment.dateTime))}</p>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <h3>By : {comment.author}</h3>
                    <span style={{ fontSize: '20px' }}>
                        {likes}
                        <LikeOutlined style={{ margin: '5px', color: isLiked && "#40A9FF" }} onClick={() => like()} />
                    </span>
                </div>
                <p>{comment.text}</p>
            </Card>


        </>
    )
}