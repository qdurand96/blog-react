import { Input } from 'antd';



export function SearchBar(){

    const { Search } = Input;

    const onSearch = value => console.log(value);

    return(
        <>
        <Search placeholder="Title or author" allowClear onSearch={onSearch} style={{ width: 200 }} />
        </>
    )
}