import { Menu } from 'antd';
import { HomeOutlined, FileOutlined, UserOutlined } from '@ant-design/icons';
import { Link, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../store/auth-slice';

export function Nav() {

    const dispatch = useDispatch()
    const user = useSelector(state => state.auth.user)

    const location = useLocation();

    return (
        <Menu selectedKeys={[location.pathname]} mode="horizontal">

            <Menu.Item key="/" icon={<HomeOutlined />}>
                <Link to="/">Home</Link>
            </Menu.Item>
            {user ?
                <>
                    <Menu.Item key="/myposts" icon={<FileOutlined />}>
                        <Link to="/myposts">My Posts</Link>
                    </Menu.Item>

                    <Menu.Item key="/account" icon={<UserOutlined />}>
                        <Link to="/account">Account</Link>
                    </Menu.Item>
                    <Menu.Item key="/logout" icon={<UserOutlined />} onClick={() => dispatch(logout())}>
                        Logout
                    </Menu.Item>
                </> :
                <>
                    <Menu.Item key="/register" icon={<UserOutlined />}>
                        <Link to="/register">Register</Link>
                    </Menu.Item>

                    <Menu.Item key="/login" icon={<UserOutlined />}>
                        <Link to="/login">Login</Link>
                    </Menu.Item>
                </>
                }
            {/* <Menu.Item key="/search" style={{marginTop:'10px'}} >
                <SearchBar/>
            </Menu.Item> */}




        </Menu>

    );

}