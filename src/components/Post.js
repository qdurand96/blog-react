import { Card } from "antd";
import { useHistory } from "react-router-dom";


export function Post({ post }) {

    const history = useHistory();

    function handleClick() {
        history.push(`/post/${post.id}`);
    }

    return (
        <>
            <Card hoverable title={post.title} style={{  marginTop:'10px' }} onClick={handleClick}>
            <p>{new Intl.DateTimeFormat('fr-FR', { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: "2-digit", minute: "2-digit", second: "2-digit" }).format(new Date(post.dateTime))}</p>
                <p>By : {post.author}</p>
                
            </Card>
        </>
    )
}