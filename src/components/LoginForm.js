
import { Form, Input, Button } from 'antd';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { loginWithCredentials } from '../store/auth-slice';



export function LoginForm() {
    const dispatch = useDispatch();
    const feedback = useSelector(state => state.auth.loginFeedback);
    const [redirect, setRedirect] = useState(false)

    const onFinish = (values) => {
        dispatch(loginWithCredentials(values));
        if (feedback === 'Logged in') {
            setRedirect(true)
        }
    }

    return (
        <>
            {redirect ? (<Redirect push to="/" />) : null}
            <Form
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 10,
                }}
                onFinish={onFinish}
            >
                {feedback && <p>{feedback}</p>}
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Email is required',
                        },
                        {
                            type: 'email',
                            message: 'Please enter a valid email',
                        },
                    ]}
                >
                    <Input type="email" />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Password is required',
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>



                <Form.Item
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        Login
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}
