import { useCallback, useEffect } from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { PostService } from "../services/PostService";
import { Button, Col, Row } from "antd";
import { useDispatch } from "react-redux";
import { PostForm } from "./PostForm";
import { deletePost, updatePost } from "../store/post-slice";
import { Redirect } from 'react-router-dom';
import { CommentForm } from "./CommentForm";
import { CommentService } from "../services/CommentService";
import { Comment } from "./Comment";
import { LikeOutlined } from '@ant-design/icons';


export function PostDetails() {

    let { id } = useParams();

    const [post, setPost] = useState(null)
    const [comments, setComments] = useState([])
    const [open, setOpen] = useState(false)
    const [redirect, setRedirect] = useState(false)
    const [isLiked, setIsLiked] = useState(false)
    const dispatch = useDispatch()
    const user = useSelector(state => state.auth.user)

    const fetchPost = useCallback(async () => {
        const post = await PostService.getOnePost(id)
        setPost(post)
    }, [id])

    const fetchComments = useCallback(async () => {
        const comments = await PostService.getPostComments(id)
        setComments(comments)
    }, [id])

    const fetchLike = useCallback(async () => {
        const liked = await PostService.isLiked(id)
        setIsLiked(liked)
    }, [id])

    useEffect(() => {
        fetchPost();
        fetchComments()
        if(user){
        fetchLike()
        }
    },[fetchComments, fetchLike, fetchPost,user])

    async function like() {
        await PostService.like(id)
        fetchPost()
        fetchLike()
    }

    function update(values) {
        dispatch(updatePost(post.id, values))
        setOpen(false)
        setPost({
            post,
            ...values
        })
    }

    function deleteThis() {
        dispatch(deletePost(post.id))
        setRedirect(true)
    }

    async function addComment(values) {
        values.postId = post.id;
        await CommentService.add(values)
        fetchComments()
    }

    async function updateCom(commentId, comment) {
        await CommentService.update(commentId, comment)
        fetchComments()
    }

    async function deleteCom(commentId) {
        await CommentService.delete(commentId)
        fetchComments()
    }


    return (
        <>
            {redirect ? (<Redirect push to="/" />) : null}

            {!post ?
                <p>Loading</p> :
                <>

                    <Row style={{ marginTop: '10px' }}>
                        <Col span={8} offset={8}>
                        <p>{new Intl.DateTimeFormat('fr-FR', { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: "2-digit", minute: "2-digit", second: "2-digit" }).format(new Date(post.dateTime))}</p>
                            
                                <div>
                                    {user && user.id === post.userId &&
                                        <>
                                            {open ? <PostForm onSubmit={update} /> : <Button onClick={() => setOpen(true)}>Update</Button>}
                                            <Button onClick={() => deleteThis()}>X</Button>
                                        </>
                                    }
                                </div>
                            
                            <h1 style={{ textAlign: 'center' }}>{post.title}</h1>
                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                <h3>By : {post.author}</h3>
                                <span style={{ fontSize: '20px' }}>
                                    {post.like}
                                    <LikeOutlined style={{ margin: '5px', color: isLiked && "#40A9FF" }} onClick={() => like()} />
                                </span>
                            </div>
                            {post.picture &&
                                <img src={post.picture} alt="" />
                            }
                            <p>{post.text}</p>
                        </Col>
                    </Row>
                    
                    <CommentForm onSubmit={addComment} />
                    <Row>

                    </Row>
                    {comments.map(item =>
                        <Col key={item.id} xs={{ span: 20, offset: 2 }} md={{ span: 12, offset: 6 }}>
                            <Comment comment={item} onUpdate={updateCom} onDelete={deleteCom} />
                        </Col>)}
                </>
            }

        </>
    )
}