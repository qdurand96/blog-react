import { Button } from "antd";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addPost } from "../store/post-slice";
import { PostForm } from "./PostForm";



export function NewPost() {

    const [open, setOpen] = useState(false)


    const dispatch = useDispatch()

    function post(values) {
        dispatch(addPost(values))
        setOpen(false)
    }


    return (
        <div style={{marginTop: '10px'}}>
            {open ? <PostForm onSubmit={post} /> :
                <div style={{display:'flex', justifyContent:'center'}}>
                    <Button style={{ width: '300px'}} onClick={() => setOpen(true)}>New Post</Button>
                </div>
            }
        </div>
    )
}