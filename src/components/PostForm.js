import { Form, Input, Button } from 'antd';



export function PostForm({onSubmit}) {


    const onFinish = (values) => {
        onSubmit(values)
    }

    return (
        <Form
            name="basic"
            labelCol={{
                span: 4,
            }}
            wrapperCol={{
                span: 16,
            }}
            onFinish={onFinish}
        >
            <Form.Item label="Title"
                name="title"
                rules={[
                    {
                        required: true,
                        message: 'Title is required',
                    }
                ]}>
                <Input />
            </Form.Item>

            <Form.Item label="Text"
                name="text"
                rules={[
                    {
                        required: true,
                        message: 'Text is required',
                    }
                ]}>
                <Input.TextArea />
            </Form.Item>

            <Form.Item
                style={{display:'flex', justifyContent:'center'}}
                wrapperCol={{
                    offset: 7,
                    span: 8,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Post
                </Button>
            </Form.Item>
        </Form>
    )
}
