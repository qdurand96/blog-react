import { Form, Input, Button } from 'antd';



export function CommentForm({onSubmit}) {


    const onFinish = (values) => {
        onSubmit(values)
    }

    return (
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 8,
            }}
            onFinish={onFinish}
        >
            
            <Form.Item label="Comment"
                name="text"
                rules={[
                    {
                        required: true,
                        message: 'Text is required',
                    }
                ]}>
                <Input.TextArea />
            </Form.Item>

            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Post
                </Button>
            </Form.Item>
        </Form>
    )
}
